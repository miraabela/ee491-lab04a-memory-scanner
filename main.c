///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04 - Memory Scanner
///
/// @file main.c
/// @version 1.0
///
/// @author Mirabela Medallon <mirabela@hawaii.edu>
/// @brief  Lab 04 - Memory Scanner - EE 491F - Spr 2021
/// @date   Feb 2, 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <setjmp.h>
#include  <signal.h>

int countLines(FILE * fp);
bool isWhitespace1(char ch);
bool isWhitespace2(char ch);
void processSegment(char start[], char end[], char perm[], int index);
void sigBusHandler(int sig);
int numLines = 0;
bool busError = false;
jmp_buf  JumpBuffer;

int main(int argc, const char * argv[]) {
   
   // TESTING: Open file, check for errors
   FILE *fp = fopen("/proc/self/maps", "r");
   if (fp == NULL) {
      printf("memscan: Can't open [%s]\n", "/proc/self/maps");
      exit(EXIT_FAILURE);
   }
   
   numLines = countLines(fp);
   char segments[numLines+1][4][17];
   memset(segments, 0, sizeof(segments));
   rewind(fp);
   // Parse each line
   char line[255];
   for (int currLine=0; currLine <= numLines; currLine++) {
      fgets(line, 255, fp);
      int words = 0;
      int currWordCharCount = 0;
      int currChar = 0;
      
      // GET STARTING ADDRESS
      while (line[currChar] != '-') {
         currWordCharCount++;
         segments[currLine][words][currWordCharCount-1] = line[currChar];
         currChar++;
      }
      words++;
      currWordCharCount = 0;
      currChar++;
      
      // GET ENDING ADDRESS
      while (line[currChar] != ' ') {
         currWordCharCount++;
         segments[currLine][words][currWordCharCount-1] = line[currChar];
         currChar++;
      }
      words++;
      currWordCharCount = 0;
      currChar++;
      
      // GET PERMISSIONS
      for (int i = 0; i < 4; i++) {
         currWordCharCount++;
         segments[currLine][words][currWordCharCount-1] = line[currChar];
         currChar++;
      }
      words++;
      currWordCharCount=0;
   }
   
   signal(SIGBUS, sigBusHandler);
   
   // PROCESS ALL SEGMENTS
   for (int i = 0; i <= numLines; i++) {
      size_t len = strlen(segments[i][0]);
      if (setjmp(JumpBuffer) == 0) {
         if (busError) {
            i--;
            printf("%d: 0x%s - 0x%s %s Cannot Read Segment\n", i, segments[i][0], segments[i][1], segments[i][2]);
            busError = false;
            i++;
         }
         if (segments[i][2][0] == 'r' && len < 14 && busError == false) {
            processSegment(segments[i][0], segments[i][1], segments[i][2], i);
         } else {
            printf("%d: 0x%s - 0x%s %s Cannot Read Segment\n", i, segments[i][0], segments[i][1], segments[i][2]);
         }
      }
      
   }
   
   fclose(fp);
   exit(EXIT_SUCCESS);
}

int countLines(FILE * fp){
   bool inWord = false;
   char curr;
   int currLines = 0;
   while ((curr = fgetc(fp)) != EOF) {
      if (isWhitespace1(curr)){
         if (inWord) {
            inWord = false;
         }
         if (isWhitespace2(curr)) currLines++;
      } else {
         inWord = true;
      }
   }
   return currLines;
}

bool isWhitespace1(char ch) {
   if (ch == ' ' || ch == '\t' || ch == '\0' || ch == '\n' || ch == '\f' || ch == '\r' || ch == '\v') {
      return true;
   } else {
      return false;
   }
}

bool isWhitespace2(char ch) {
   if (ch == '\0' || ch == '\n') {
      return true;
   } else {
      return false;
   }
}


void processSegment(char start[], char end[], char perm[], int index) {
   // convert Hex string to pointer
   char *currByte;
   char *endByte;
   long startAddr = strtol(start, NULL, 16);
   long endAddr = strtol(end, NULL, 16);
   currByte = (char *)startAddr;
   endByte = (char *)endAddr;
   
   // Start reading
   int bytesRead = 0;
   int countA = 0;
   
   for (; currByte < endByte; currByte++) {
      bytesRead++;
      if (*currByte == 'A') countA++;
   }
   
   // Print stats
   printf("%d: 0x%s - 0x%s %s Bytes read [%d] 'A' count [%d]\n", index, start, end, perm, bytesRead, countA);
   
}

void sigBusHandler(int sig) {
   busError = true;
   longjmp(JumpBuffer, 1);
}


