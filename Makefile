# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = memscan

all: $(TARGET)

memscan: main.c
	$(CC) $(CFLAGS) -o $(TARGET) main.c

clean:
	rm $(TARGET)

test:
	./$(TARGET)



